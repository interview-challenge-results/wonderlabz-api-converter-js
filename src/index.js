const express = require('express')
const app = express()
const port = 3000

app.get('/conversions/ktoc/:value', (req, res) => {
  console.log('[GET] /conversions/ktoc/')
  console.time('ktoc')
  const { value } = req.params
  if (!value) {
    res.status(500).send('no-value')
    return
  }
  const converted = kelvinToCelcius(value)
  console.timeEnd('ktoc')
  res.status(200).send(converted)
})

app.get('/conversions/ctok/:value', (req, res) => {
  console.log('[GET] /conversions/ctok/')
  console.time('ctok')
  const { value } = req.params
  if (!value) {
    res.status(500).send('no-value')
    return
  }
  const converted = celciusToKelvin(value)
  console.timeEnd('ctok')
  res.status(200).send(converted)
})

app.get('/conversions/mtok/:value', (req, res) => {
  console.log('[GET] /conversions/mtok/')
  console.time('mtok')
  const { value } = req.params
  if (!value) {
    res.status(500).send('no-value')
    return
  }
  const converted = milesToKilos(value)
  console.timeEnd('mtok')
  res.status(200).send(converted)
})

app.get('/conversions/ktom/:value', (req, res) => {
  console.log('[GET] /conversions/ktom/')
  console.time('ktom')
  const { value } = req.params
  if (!value) {
    res.status(500).send('no-value')
    return
  }
  const converted = kilosToMiles(value)
  console.timeEnd('ktom')
  res.status(200).send(converted)
})

app.listen(port, () => {
  console.log(
    `Converter API listening for requests at http://localhost:${port}`
  )
})

function kelvinToCelcius(value) {
  return `${getInt(value) - 273.15}c`
}

function celciusToKelvin(value) {
  return `${getInt(value) + 273.15}k`
}

function milesToKilos(value) {
  return `${getInt(value) * 1.609344}km`
}

function kilosToMiles(value) {
  return `${getInt(value) / 1.609344}mi`
}

/**
 * Converts a string to an int, returns 0 if conversion fail
 * @param {string} value Value to be parsed
 */
function getInt(value) {
  const parsed = parseInt(value)
  if (isNaN(parsed)) {
    return 0
  }
  return parsed
}
