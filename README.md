# API Converter (Cross Platform)
![main-interface](/images/main-interface.png)

When starting the application, you will be presented with a main console output screen showing where the API can be accessed

![logging](/images/logging.png)

Whenever an endpoint is hit, a new log will be generated showing which endpoint was hit, and the time it took for the conversion calculation to complete.

## Running
In order to run this application, you will need NodeJS (v12) installed on your system.
Open a terminal instance in the root of this project, and run the following commands:  
`npm i` to install all necessary libraries  
`npm start` to start the game  

Alternatively, you can run the game by downloading and running the binary file (linked below) for your system.

## Endpoints
`[GET]` `/conversions/ktoc/:value`  
Convert the `value` given from **kelvin** to **celcius**  

`[GET]` `/conversions/ctok/:value`  
Convert the `value` given from **celcius** to **kelvin**  

`[GET]` `/conversions/mtok/:value`  
Convert the `value` given from **miles** to **kilometers**  

`[GET]` `/conversions/ktom/:value`  
Convert the `value` given from **kilometers** to **miles**  

## HTTP Location
You can query the endpoints listed above by navigating your browser to:  
`http://localhost:3000/{endpoint}`

## Links
Windows Executable File:  
https://drive.google.com/file/d/19Ycs3iUmyJjNq6cUkO4wtH9z1axSEjAn/view?usp=sharing

MacOS Executable File:  
https://drive.google.com/file/d/130F6PJnjEgtnlWguXmd8o6N5sjDbqds3/view?usp=sharing